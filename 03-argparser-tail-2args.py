#!/usr/bin/python
#-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Exemple: argarparser - tail [-f fitxer] [-n linies]
# 
# Valida la següent synopsis, on per defecte es processen les 10 ultimes linies de stin
#
# Synopsis: $ tail.py [-f fitxer] [-n linies]
#-------------------------------------------------------------

import argparse, sys

parser = argparse.ArgumentParser(description="tail [-f file]")
parser.add_argument("-f", "--fitxer", dest="argFile", type=str, default="/dev/stdin")
parser.add_argument("-n", "--numero", dest="argLine", type=int, default=10)
args=parser.parse_args()
print args
print args.argFile
print args.argLine
sys.exit(0)

# default=10 comentem que si no rebrem per STDIN altre numero, per defecte sera el numero 10
