#!/usr/bin/python
#-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Exemple: fork/signal
# Un pare genera un fill i es mor.
# El fill fa un bucle infinit, però:
# 	. acaba als 3 minuts
#	. amb sithub torna a començar els tres minuts
#	. amb sigterm mostra missatge(segons restants) i plega
#-------------------------------------------------------------

import signal, os, sys

def handlerTerm(signum,frame):
    print signal.alarm(0)
    sys.exit(0)

def handlerHup(signum,frame):
    print "retornem a tres minuts"
    signal.alarm(3*60)

signal.signal(signal.SIGTERM,handlerTerm)
signal.signal(signal.SIGHUP,handlerHup)
signal.alarm(3*60)

pid = os.fork()

if pid != 0:
    print "Programa PARE mort"
    sys.exit(0)
else:
    print os.getpid()
    while True:
        pass
sys.exit(0)

