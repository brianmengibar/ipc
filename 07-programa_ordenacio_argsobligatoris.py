#!/usr/bin/python
#-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Exemple: argarparser - [-s login | uid | gid | gname] -u usuaris -g grups
# 
# Valida la següent synopsis que rep obligatoriament les opcions dels fitxers d'usuaris i grups per processar-los segons el criteri per defecte login
#
# Synopsis: $ programa [-s login|uid|gid|gname] usuaris grup
#-------------------------------------------------------------

import argparse, sys

parser = argparse.ArgumentParser(description="programa [-s login|uid|gid|gname] -u usuaris -g grup")
parser.add_argument("-s", "--sort", dest="argList", type=str, default="login", metavar='login|uid|gid|gname', choices=["login","uid","gid","gname"])
parser.add_argument("usuaris")
parser.add_argument("groups")
args=parser.parse_args()
print args
print args.argList
sys.exit(0)

# No hi ha que posar required=True, ja que no hi ha un positional especificat, al no posar res, per defecte es True

