#!/usr/bin/python
#-*- coding: utf-8-*-
# exemple de telnet client
# @isx39441584
# Brian Mengibar Garcia
# Synopsis: telnet_client.py -s -p
#-----------------------------------------------------------------------

# Importem
import sys,socket,argparse

# Creació arguments
parser = argparse.ArgumentParser(description="telnet_client.py -s server -p port")
parser.add_argument("-s", "--server", dest="hostConnect", required=True)
parser.add_argument("-p", "--port", dest="portConnect", type=int, required= True) 

args = parser.parse_args()
HOST = args.hostConnect
PORT = args.portConnect

# Llistat de paraules "prohibides"
list_closeconnection=["quit","fi","close","exit"]

# Creo socket i em connecto
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

# Bucle infinit on si le enviem una paraula prohibida sortira, si no
# enviem res queda esperant fins que envii alguna ordre
while True:
	command = raw_input("telnet > ")
	if command in list_closeconnection:
		break
	if not command:
		continue
	s.send(command)
	# Bucle infinit que rep el contigut del popen generat pel server on
	# si rep el character 4 plega
	while True:
		data = s.recv(1024)
		if data[-1] == chr(4):
			print data[:-1]
			break
		print data,
s.close()
sys.exit(0)
