#!/usr/bin/python
#-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Exemple: fork/signal
# Un pare genera un fill i es mor.
# El fill executa el programa: 06-signal.py
#-------------------------------------------------------------

import signal, os, sys

pid = os.fork()

if pid != 0:
    print "Programa PARE mort"
    sys.exit(0)
else:
    os.execv("/usr/bin/python",["/usr/bin/python","06-signal.py","5"])
sys.exit(0)

