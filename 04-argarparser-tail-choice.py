#!/usr/bin/python
#-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Exemple: argarparser - tail [-f fitxer] [-n 5| 10 | 15 ]
# 
# Valida la següent synopsis, on per defecte es processen les 10 ultimes linies de stin
#
# Synopsis: $ tail.py [-f fitxer] [-n 5 | 10 | 15 ]
#-------------------------------------------------------------

import argparse, sys

parser = argparse.ArgumentParser(description="tail [-f file]")
parser.add_argument("-f", "--file", dest="argFile", type=str, default="/dev/stdin")
parser.add_argument("-n", "--number", dest="argLine", type=int, default=10, metavar='5|10|15', choices=[5,10,15])
args=parser.parse_args()
print args
print args.argFile
print args.argLine
sys.exit(0)

# default=10 comentem que si no rebrem per STDIN altre numero, per defecte sera el numero 10
# choices=["5","10","15"] podem escollir un d'aquests tres, si no escollim per defecte sera 10
