#!/usr/bin/python
#-*- coding: utf-8-*-
# exemple de echo server basic
#-----------------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX hisix2 M06-ASO UF2NF1-Scripts
# @isx39441584 Curs 2016-2017 Gener 2016
#-----------------------------------------------------------------------

# Importem els moduls sys i socket
import sys,socket

# Al ficar '' ens referim a localhost
HOST = ''

# Especifiquem el port que volem
PORT = 5001

# Crea un objecte socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect((HOST, PORT))
# Bucle infinit
while True:
	# Processa les dades per l'entrada standard
	dataIn = raw_input()
	# Si es pasa una d'aquestes paraules, finalitza la connexio
	if dataIn in ["FI", "fi", "EXIT", "exit"]: break
	# Envio les dades
	s.send(dataIn)
	# Rep les dades
	#dataRecv = s.recv(1024) #aixo en un exam es un zero
	# Mostro les dades
	print 'Received:', s.recv(1024)
s.close()
sys.exit(0)
