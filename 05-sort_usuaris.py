#!/usr/bin/python
#-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Exemple: argarparser - sort [-s login|uid|name] [-f fitxer]
# 
# Valida la següent synopsis on per defecte es processa stdin amb el criteri d'ordenació per login
#
# Synopsis: $ sort_usuaris.py [-s login|uid|name] [-f fitxer]
#-------------------------------------------------------------

import argparse, sys

parser = argparse.ArgumentParser(description="sort_usuaris [-s login|uid|name] [-f fitxer]")
parser.add_argument("-s", "--sort", dest="argList", type=str, default="login", metavar='login|uid|name', choices=["login","uid","name"])
parser.add_argument("-f", "--file", dest="argFile", type=str, default="/dev/stdin")
args=parser.parse_args()
print args
print args.argFile
print args.argList
sys.exit(0)

