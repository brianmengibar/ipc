#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @isx39441584 Curs 2017-2018 Febrer 2017
# -----------------------------------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr
while True:
    data = conn.recv(1024)
    if not data: break # no hi ha data quan data val none, quan pasa aixo? quan data=conn.recv(1024) S'HA TRENCAT
    conn.send(data)
conn.close()
sys.exit(0)
