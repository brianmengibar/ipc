#!/usr/bin/python
#-*- coding: utf-8-*-
# exemple de daytime client
# @isx39441584
#-----------------------------------------------------------------------

# Importem
import sys, socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 5001

# Creem socket i l'indiquem a quin host i port te que connectarse
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

command = ["/usr/bin/nmap","localhost"]

pipeData=Popen(command, stdout=PIPE, stderr=PIPE)
for line in pipeData.stdout:
        s.send(line)
s.close()
sys.exit(0)
