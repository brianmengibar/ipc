#!/usr/bin/python 
#-*- coding: utf-8-*-
# @isx39441584
# Brian Mengibar Garcia
# Deures: 
# 	- Fer mateix programa que 16 pero que sigui un fitxer per cada client, no per cada conexio.
# el client fa ps ax (originariament feia nmap) i envia les dades al server.
# el server és OneByOne i desa les dades a un fitxer ip.log.
# les dades de cada client s'acumulen en un fitxer, tantes connexions com faci un mateix client 
# s'acumulen en un mateix fitxer.
# un fitxer diferent per a cada client (no per a cada connexió).

import sys,socket,os
import time
HOST = ''
PORT = 5001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
print os.getpid()
while True:
	try:
		conn, addr = s.accept()
	except Exception:
		continue
	ip, port = addr
	sys.stderr.write("Connected by: %s \n" % ip)
	filename = "{}.log".format(ip)
	fitxer = open(filename, 'a')
	while True:
		data = conn.recv(1024)
		if not data:
			fitxer.close()
			break
		fitxer.write(data)
	conn.close()
sys.exit(0)
