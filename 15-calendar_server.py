#!/usr/bin/python
#-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Calendar server
#----------------------------

import os,sys,socket,signal
from subprocess import Popen, PIPE

# Daemon
pid=os.fork()

if pid != 0:
    print "Programa PARE mort"
    sys.exit(0)
else:
    print "Programa fill comença el programa\n"

# Signals
listClients = []

def handlerUsr1(signum, frame):
	global listClients
	print listClients

def handlerUsr2(signum,frame):
	global listPeers
	print "Total de connexions:", len(listClients)

def handlerAlarm(signum, frame):
    print "s'ha acabat el teu temps!"
    sys.exit(0)

def handlerTerm(signum, frame):
    print "no surts, no surts!"

def handlerHup (signum, frame):
	global listClients
	listClients = []

signal.signal(signal.SIGALRM, handlerAlarm)
signal.signal(signal.SIGUSR1, handlerUsr1)
signal.signal(signal.SIGUSR2, handlerUsr2)
signal.signal(signal.SIGTERM, handlerTerm)
signal.signal(signal.SIGHUP, handlerHup)

# Creem socket i declarem a que host i a que port te que conectarse
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
HOST = ''
PORT = 5001
s.bind((HOST, PORT))
s.listen(1)
print os.getpid()

# Bucle infinit on generem la tuberia que enviará el contigut al client
while True:
    try:
        conn, addr = s.accept()
    except:
        continue
    #sys.stderr.write("Connected by %s" % addr)
    listClients.append(addr)
    year = conn.recv(1024)
    command = ["/usr/bin/cal -y %s" % year]
    pipeData=Popen(command, shell = True, stdout=PIPE, stderr=PIPE)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()
sys.exit(0)
