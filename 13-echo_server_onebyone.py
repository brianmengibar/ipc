#!/usr/bin/python
#-*- coding: utf-8-*-
# exemple de echo server one by one
#-----------------------------------------------------------------------
# Escola del treball de Barcelona
# @isx39441584 Curs 2017-2018 Febrer 2018
#-----------------------------------------------------------------------

# Importem els moduls os, sys i socket
import os,sys,socket

HOST = ''
PORT = 5001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)

print os.getpid()

# Escolta indefinidament
while True:
    conn, addr = s.accept()
    # Fem aquest print per sapiguer quin es connecta (IP i port)
    print 'Connected by' , addr
    # bucle infinit
    while True:
    	data = conn.recv(1024)
    	if not data: break
    	conn.send(data)
    conn.close()
sys.exit(0)
