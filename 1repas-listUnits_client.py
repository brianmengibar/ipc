#!/usr/bin/python
#-*- coding: utf-8-*-
# Exemple server realitza un systemctl list-units i l’envia al client.
# @isx39441584
# Brian Mengibar Garcia
# Synopsis: 1repas-listUnits_client -s -p
#---------------------------------------------------------------------

import sys,socket,argparse

# Creació arguments
parser = argparse.ArgumentParser(description="1repas-listUnits_clientpy -s server -p port")
parser.add_argument("-s", "--server", dest="hostConnect", required=True)
parser.add_argument("-p", "--port", dest="portConnect", type=int, required= True)

args = parser.parse_args()
HOST = args.hostConnect
PORT = args.portConnect

# Crea un objecte socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

# Bucle infinit
while True:
	# Rep les dades
	data = s.recv(1024)
	if not data: break
	# Mostrem les dades
	sys.stdout.write(data)
s.close()
sys.exit(0)
