#!/usr/bin/python
# @isx39441584 Curs 2017-2018
#
# Exemple IPC: Signals
#-----------------------------

import signal, os, sys
def myhandler(signum,frame):
	print "Signal handler called with signal", signum
	print "Hasta luego Lucas"
	sys.exit(1)

def mydeath(signum,frame):
        print "Signal handler called with signal", signum
        print "que te mueras tu!!!"

def mymulti(signum,frame):
    for i in range(1,10):
        print '\n'
        for j in range(1,11):
            print j*i

def myviscallei(signum,frame):
    print "Visca la llei 155!!"

signal.signal(signal.SIGALRM,myhandler)
signal.signal(signal.SIGTERM,mydeath)
signal.signal(signal.SIGUSR1,mymulti)
signal.signal(signal.SIGUSR2,myviscallei)
signal.alarm(60)

while True:
	#Fem un Popen que es queda encallat
	pass

signal.alarm(0)
sys.exit(0)
