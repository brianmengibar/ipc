#!/usr/bin/python
#-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Exemple: argarparser - head
# 
# Valida la següent synopsis, on per defecte es processa stdin
#
# Synopsis: $ head [file]
#-------------------------------------------------------------

import argparse, sys

parser = argparse.ArgumentParser(description="head [file]")
parser.add_argument("file", type=str, nargs='?', default="/dev/stdin")
args=parser.parse_args()
print args
sys.exit(0)

# nargs = nombre d'arguments que s'han de consumir (en aquest cas per defecte per stdin
