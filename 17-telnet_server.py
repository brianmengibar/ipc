#!/usr/bin/python
#-*- coding: utf-8-*-
# exemple de telnet server
# @isx39441584
# Brian Mengibar Garcia
# Synopsis: telnet_server.py -s -p
#-----------------------------------------------------------------------

# Importem
import sys,socket,os,argparse,signal
from subprocess import Popen, PIPE

# Daemon
pid=os.fork()

if pid != 0:
	print "Programa PARE mort"
	sys.exit(0)
else:
	print "Programa fill comença el programa\n"

# Signals
listClients = []

def handlerUsr1(signum, frame):
	global listClients
	print "Llistat de clients: %s" % (listClients)

def handlerUsr2(signum,frame):
	global listPeers
	print "Total de connexions:", len(listClients)

def handlerAlarm(signum, frame):
    print "s'ha acabat el teu temps!"
    sys.exit(0)

def handlerTerm(signum, frame):
    print "no surts, SIGKILL please"

def handlerHup (signum, frame):
	global listClients
	listClients = []

signal.signal(signal.SIGALRM, handlerAlarm)
signal.signal(signal.SIGUSR1, handlerUsr1)
signal.signal(signal.SIGUSR2, handlerUsr2)
signal.signal(signal.SIGTERM, handlerTerm)
signal.signal(signal.SIGHUP, handlerHup)
signal.signal(signal.SIGINT, signal.SIG_IGN)

# Creació arguments
parser = argparse.ArgumentParser(description="telnet_server.py -s server -p port")
parser.add_argument("-s", "--server", dest="hostConnect", required=True)
parser.add_argument("-d", "--debug", dest="debugStdin", action="store_true")
parser.add_argument("-p", "--port", dest="portConnect", type=int, required= True) 

args = parser.parse_args()
HOST = args.hostConnect
PORT = args.portConnect
debug = args.debugStdin

# Llistat de paraules "prohibides"
list_closeconnection=["quit","fi","close","exit"]

# Creem socket i declarem a que host i a que port te que conectarse
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
print os.getpid()

# Bucle infinit que accepta connexio amb client
while True:
	try:
		conn, addr = s.accept()
	except Exception:
		continue
	ip, port = addr
	sys.stderr.write("Connected by: %s \n" % ip)
	listClients.append(addr)
	# Bucle infinit que rep l'ordre. Si no rep res o es una paraula prohibida? Pleguem
	while True:
		command = conn.recv(1024)
		if not command or command in list_closeconnection:
			break
		print command
		# Creem popen, enviem el contingut del popen al client
		pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE,stdin=PIPE)
		for line in pipeData.stdout:
			if debug: print line
			conn.send(line)
		# Enviem el character 4, YATA!!
		conn.send(chr(4))
	conn.close()
sys.exit(0)
