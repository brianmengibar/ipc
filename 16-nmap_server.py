#!/usr/bin/python
#-*- coding: utf-8-*-
# exemple de daytime onebyone
# @isx39441584
#-----------------------------------------------------------------------

# Improtem
import os,sys,socket,time

HOST = ''
PORT = 5001

# Creem socket i declarem a que host i a que port te que conectarse
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
print os.getpid()

# Bucle infinit on generem la tuberia que enviará el contigut al client
while True:
    try:
        conn, addr = s.accept()
    except Exception:
        continue
    timestamp=time.strftime("%Y%m%d%H%M%s")
    ipaddr=addr[0]
    file_save="%s-%s.txt" % (ipaddr,timestamp)
    fitxer=open(file_save,'w')
    while True:
        data = conn.recv(1024)
        if not data:
            fitxer.close()
            break
        fitxer.write(data)
        #print "--%s--%d--" % (data, ord(data[0]))
    conn.close()
sys.exit(0)
