#!/usr/bin/python
##-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Exemple IPC: Signals
# Synopsis: alarm.py -m minuts
#-----------------------------

import signal, os, sys, argparse

parser = argparse.ArgumentParser(description='Gestionar alarma')
parser.add_argument("minuts", type=int, help='Minuts a especificar')
args=parser.parse_args()
print parser
print os.getpid()
upper=0
down=0

def handlerUsr1(signum,frame):
    global upper
    upper+=1
    segons=signal.alarm(0)
    print segons
    signal.alarm(args.minuts*60)

def handlerUsr2(signum,frame):
    global down
    down+=1
    segons=signal.alarm(0)
    print segons
    signal.alarm(args.minuts*60)

def handlerAlarm(signum,frame):
    segons=signal.alarm(0)
    print segons
    signal.alarm(segons)

def handlerTerm(signum,frame):
    print args.minuts, signal.alarm(0), upper, down

def handlerHup(signum,frame):
    signal.alarm(args.minuts*60)
    print args.minuts*60

signal.signal(signal.SIGTERM,handlerTerm)
signal.signal(signal.SIGUSR1,handlerUsr1)
signal.signal(signal.SIGUSR2,handlerUsr2)
signal.signal(signal.SIGALRM,handlerAlarm)
signal.signal(signal.SIGHUP,handlerHup)
signal.signal(signal.SIGINT, signal.SIG_IGN)
signal.alarm(args.minuts*60)

while True:
	#Fem un Popen que es queda encallat
	pass
signal.alarm(0)
sys.exit(0)
