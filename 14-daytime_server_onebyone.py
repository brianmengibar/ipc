#!/usr/bin/python
#-*- coding: utf-8-*-
# exemple de daytime onebyone
# @isx39441584
#-----------------------------------------------------------------------

# Improtem
import os,sys,socket
from subprocess import Popen,PIPE

HOST = ''
PORT = 5000

# Creem socket i declarem a que host i a que port te que conectarse
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
print os.getpid()

# Bucle infinit on generem la tuberia que enviará el contigut al client
while True:
	conn, addr = s.accept()
	print "Connected by: ", addr
	command = ["/usr/bin/date"]
	pipeData=Popen(command, stdout=PIPE, stderr=PIPE)
	for line in pipeData.stdout:
		conn.send(line)
	conn.close()
sys.exit(0)

