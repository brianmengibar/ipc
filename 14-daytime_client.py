#!/usr/bin/python
#-*- coding: utf-8-*-
# exemple de daytime client
# @isx39441584
#-----------------------------------------------------------------------

# Importem
import sys, socket

HOST = ''
PORT = 5000

# Creem socket i l'indiquem a quin host i port te que connectarse
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

# Bucle que rep el resultat d'executar el popen en el server i plega
while True:
	data = s.recv(1024)
	if not data: break
	print data
s.close()
sys.exit(0)
