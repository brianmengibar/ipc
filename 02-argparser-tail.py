#!/usr/bin/python
#-*- coding: utf-8-*-
# @isx39441584 Curs 2017-2018
#
# Exemple: argarparser - tail
# 
# Valida la següent synopsis, on per defecte es processa stdin
#
# Synopsis: $ tail.py [-f fitxer]
#-------------------------------------------------------------

import argparse, sys

parser = argparse.ArgumentParser(description="tail [-f file]")
parser.add_argument("-f", "--fitxer", dest="argFile", type=str, default="/dev/stdin")
args=parser.parse_args()
print args
print args.argFile
sys.exit(0)

